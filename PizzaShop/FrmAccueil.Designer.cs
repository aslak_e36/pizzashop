﻿namespace PizzaShop
{
    partial class FrmAccueil
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmAccueil));
            this.cmdCloseApp = new System.Windows.Forms.Button();
            this.picLogo = new System.Windows.Forms.PictureBox();
            this.lblBienvenue = new System.Windows.Forms.Label();
            this.lstOrders = new System.Windows.Forms.ListBox();
            this.cmdNewOrder = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // cmdCloseApp
            // 
            this.cmdCloseApp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.cmdCloseApp.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdCloseApp.FlatAppearance.BorderSize = 0;
            this.cmdCloseApp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdCloseApp.Font = new System.Drawing.Font("Corbel", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdCloseApp.ForeColor = System.Drawing.Color.OrangeRed;
            this.cmdCloseApp.Location = new System.Drawing.Point(462, 12);
            this.cmdCloseApp.Name = "cmdCloseApp";
            this.cmdCloseApp.Size = new System.Drawing.Size(43, 41);
            this.cmdCloseApp.TabIndex = 0;
            this.cmdCloseApp.Text = "X";
            this.cmdCloseApp.UseVisualStyleBackColor = false;
            this.cmdCloseApp.Click += new System.EventHandler(this.cmdCloseApp_Click);
            // 
            // picLogo
            // 
            this.picLogo.Image = ((System.Drawing.Image)(resources.GetObject("picLogo.Image")));
            this.picLogo.Location = new System.Drawing.Point(163, 12);
            this.picLogo.Name = "picLogo";
            this.picLogo.Size = new System.Drawing.Size(177, 170);
            this.picLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picLogo.TabIndex = 1;
            this.picLogo.TabStop = false;
            // 
            // lblBienvenue
            // 
            this.lblBienvenue.AutoSize = true;
            this.lblBienvenue.Font = new System.Drawing.Font("Segoe Print", 24F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBienvenue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.lblBienvenue.Location = new System.Drawing.Point(29, 185);
            this.lblBienvenue.Name = "lblBienvenue";
            this.lblBienvenue.Size = new System.Drawing.Size(444, 56);
            this.lblBienvenue.TabIndex = 2;
            this.lblBienvenue.Text = "Bienvenue chez PizzaShop";
            // 
            // lstOrders
            // 
            this.lstOrders.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.lstOrders.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lstOrders.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.lstOrders.FormattingEnabled = true;
            this.lstOrders.ItemHeight = 16;
            this.lstOrders.Location = new System.Drawing.Point(13, 251);
            this.lstOrders.Name = "lstOrders";
            this.lstOrders.Size = new System.Drawing.Size(492, 176);
            this.lstOrders.TabIndex = 3;
            // 
            // cmdNewOrder
            // 
            this.cmdNewOrder.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.cmdNewOrder.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdNewOrder.FlatAppearance.BorderSize = 0;
            this.cmdNewOrder.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdNewOrder.Font = new System.Drawing.Font("Bahnschrift", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdNewOrder.ForeColor = System.Drawing.Color.White;
            this.cmdNewOrder.Location = new System.Drawing.Point(13, 439);
            this.cmdNewOrder.Name = "cmdNewOrder";
            this.cmdNewOrder.Size = new System.Drawing.Size(492, 55);
            this.cmdNewOrder.TabIndex = 4;
            this.cmdNewOrder.Text = "Nouvelle commande";
            this.cmdNewOrder.UseVisualStyleBackColor = false;
            this.cmdNewOrder.Click += new System.EventHandler(this.cmdNewOrder_Click);
            // 
            // FrmAccueil
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.ClientSize = new System.Drawing.Size(517, 508);
            this.Controls.Add(this.cmdNewOrder);
            this.Controls.Add(this.lstOrders);
            this.Controls.Add(this.lblBienvenue);
            this.Controls.Add(this.picLogo);
            this.Controls.Add(this.cmdCloseApp);
            this.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmAccueil";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Accueil";
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button cmdCloseApp;
        private System.Windows.Forms.PictureBox picLogo;
        private System.Windows.Forms.Label lblBienvenue;
        private System.Windows.Forms.ListBox lstOrders;
        private System.Windows.Forms.Button cmdNewOrder;
    }
}


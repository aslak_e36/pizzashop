﻿namespace PizzaShop
{
    partial class FrmOrder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdValidateOrder = new System.Windows.Forms.Button();
            this.cmdCancelOrder = new System.Windows.Forms.Button();
            this.grpInfo = new System.Windows.Forms.GroupBox();
            this.lblNbOrder = new System.Windows.Forms.Label();
            this.lblLabelNbOrder = new System.Windows.Forms.Label();
            this.lblPrice = new System.Windows.Forms.Label();
            this.lblLabelPrice = new System.Windows.Forms.Label();
            this.grpPasta = new System.Windows.Forms.GroupBox();
            this.cmbPastaType = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lstComponentsChosen = new System.Windows.Forms.ListBox();
            this.grpComponentsChosen = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lstComponentsAvailable = new System.Windows.Forms.ListBox();
            this.cmdAddComponents = new System.Windows.Forms.Button();
            this.cmdRemoveComponents = new System.Windows.Forms.Button();
            this.grpInfo.SuspendLayout();
            this.grpPasta.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.grpComponentsChosen.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // cmdValidateOrder
            // 
            this.cmdValidateOrder.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.cmdValidateOrder.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdValidateOrder.FlatAppearance.BorderSize = 0;
            this.cmdValidateOrder.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdValidateOrder.Font = new System.Drawing.Font("Bahnschrift", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdValidateOrder.ForeColor = System.Drawing.Color.White;
            this.cmdValidateOrder.Location = new System.Drawing.Point(12, 468);
            this.cmdValidateOrder.Name = "cmdValidateOrder";
            this.cmdValidateOrder.Size = new System.Drawing.Size(289, 76);
            this.cmdValidateOrder.TabIndex = 5;
            this.cmdValidateOrder.Text = "Valider";
            this.cmdValidateOrder.UseVisualStyleBackColor = false;
            // 
            // cmdCancelOrder
            // 
            this.cmdCancelOrder.BackColor = System.Drawing.Color.Red;
            this.cmdCancelOrder.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdCancelOrder.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cmdCancelOrder.FlatAppearance.BorderSize = 0;
            this.cmdCancelOrder.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdCancelOrder.Font = new System.Drawing.Font("Bahnschrift", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdCancelOrder.ForeColor = System.Drawing.Color.White;
            this.cmdCancelOrder.Location = new System.Drawing.Point(307, 468);
            this.cmdCancelOrder.Name = "cmdCancelOrder";
            this.cmdCancelOrder.Size = new System.Drawing.Size(279, 76);
            this.cmdCancelOrder.TabIndex = 6;
            this.cmdCancelOrder.Text = "Annuler";
            this.cmdCancelOrder.UseVisualStyleBackColor = false;
            // 
            // grpInfo
            // 
            this.grpInfo.Controls.Add(this.lblNbOrder);
            this.grpInfo.Controls.Add(this.lblLabelNbOrder);
            this.grpInfo.Controls.Add(this.lblPrice);
            this.grpInfo.Controls.Add(this.lblLabelPrice);
            this.grpInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grpInfo.Font = new System.Drawing.Font("Bahnschrift", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpInfo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.grpInfo.Location = new System.Drawing.Point(12, 12);
            this.grpInfo.Name = "grpInfo";
            this.grpInfo.Size = new System.Drawing.Size(574, 75);
            this.grpInfo.TabIndex = 7;
            this.grpInfo.TabStop = false;
            this.grpInfo.Text = "Informations";
            // 
            // lblNbOrder
            // 
            this.lblNbOrder.AutoSize = true;
            this.lblNbOrder.Font = new System.Drawing.Font("Bahnschrift", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNbOrder.ForeColor = System.Drawing.Color.Salmon;
            this.lblNbOrder.Location = new System.Drawing.Point(68, 23);
            this.lblNbOrder.Name = "lblNbOrder";
            this.lblNbOrder.Size = new System.Drawing.Size(77, 35);
            this.lblNbOrder.TabIndex = 3;
            this.lblNbOrder.Text = "2750";
            // 
            // lblLabelNbOrder
            // 
            this.lblLabelNbOrder.AutoSize = true;
            this.lblLabelNbOrder.Font = new System.Drawing.Font("Bahnschrift", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLabelNbOrder.Location = new System.Drawing.Point(11, 23);
            this.lblLabelNbOrder.Name = "lblLabelNbOrder";
            this.lblLabelNbOrder.Size = new System.Drawing.Size(63, 35);
            this.lblLabelNbOrder.TabIndex = 2;
            this.lblLabelNbOrder.Text = "N° : ";
            // 
            // lblPrice
            // 
            this.lblPrice.AutoSize = true;
            this.lblPrice.Font = new System.Drawing.Font("Bahnschrift", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrice.ForeColor = System.Drawing.Color.Salmon;
            this.lblPrice.Location = new System.Drawing.Point(469, 23);
            this.lblPrice.Name = "lblPrice";
            this.lblPrice.Size = new System.Drawing.Size(90, 35);
            this.lblPrice.TabIndex = 1;
            this.lblPrice.Text = "0 CHF";
            // 
            // lblLabelPrice
            // 
            this.lblLabelPrice.AutoSize = true;
            this.lblLabelPrice.Font = new System.Drawing.Font("Bahnschrift", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLabelPrice.Location = new System.Drawing.Point(394, 23);
            this.lblLabelPrice.Name = "lblLabelPrice";
            this.lblLabelPrice.Size = new System.Drawing.Size(83, 35);
            this.lblLabelPrice.TabIndex = 0;
            this.lblLabelPrice.Text = "Prix : ";
            // 
            // grpPasta
            // 
            this.grpPasta.Controls.Add(this.cmbPastaType);
            this.grpPasta.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grpPasta.Font = new System.Drawing.Font("Bahnschrift", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpPasta.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.grpPasta.Location = new System.Drawing.Point(12, 93);
            this.grpPasta.Name = "grpPasta";
            this.grpPasta.Size = new System.Drawing.Size(574, 65);
            this.grpPasta.TabIndex = 8;
            this.grpPasta.TabStop = false;
            this.grpPasta.Text = "Type de pâte";
            // 
            // cmbPastaType
            // 
            this.cmbPastaType.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.cmbPastaType.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbPastaType.ForeColor = System.Drawing.Color.White;
            this.cmbPastaType.FormattingEnabled = true;
            this.cmbPastaType.Location = new System.Drawing.Point(6, 26);
            this.cmbPastaType.Name = "cmbPastaType";
            this.cmbPastaType.Size = new System.Drawing.Size(562, 27);
            this.cmbPastaType.TabIndex = 0;
            this.cmbPastaType.SelectedIndexChanged += new System.EventHandler(this.cmbPastaType_SelectedIndexChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cmdRemoveComponents);
            this.groupBox1.Controls.Add(this.cmdAddComponents);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.grpComponentsChosen);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox1.Font = new System.Drawing.Font("Bahnschrift", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.groupBox1.Location = new System.Drawing.Point(12, 164);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(574, 298);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Composition de la pizza";
            // 
            // lstComponentsChosen
            // 
            this.lstComponentsChosen.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.lstComponentsChosen.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.lstComponentsChosen.FormattingEnabled = true;
            this.lstComponentsChosen.ItemHeight = 19;
            this.lstComponentsChosen.Location = new System.Drawing.Point(6, 26);
            this.lstComponentsChosen.Name = "lstComponentsChosen";
            this.lstComponentsChosen.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.lstComponentsChosen.Size = new System.Drawing.Size(199, 232);
            this.lstComponentsChosen.TabIndex = 0;
            // 
            // grpComponentsChosen
            // 
            this.grpComponentsChosen.Controls.Add(this.lstComponentsChosen);
            this.grpComponentsChosen.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.grpComponentsChosen.Location = new System.Drawing.Point(6, 26);
            this.grpComponentsChosen.Name = "grpComponentsChosen";
            this.grpComponentsChosen.Size = new System.Drawing.Size(211, 267);
            this.grpComponentsChosen.TabIndex = 2;
            this.grpComponentsChosen.TabStop = false;
            this.grpComponentsChosen.Text = "Ingré. choisis";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lstComponentsAvailable);
            this.groupBox2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.groupBox2.Location = new System.Drawing.Point(357, 26);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(211, 267);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Ingré. disponibles";
            // 
            // lstComponentsAvailable
            // 
            this.lstComponentsAvailable.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.lstComponentsAvailable.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.lstComponentsAvailable.FormattingEnabled = true;
            this.lstComponentsAvailable.ItemHeight = 19;
            this.lstComponentsAvailable.Location = new System.Drawing.Point(6, 26);
            this.lstComponentsAvailable.Name = "lstComponentsAvailable";
            this.lstComponentsAvailable.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.lstComponentsAvailable.Size = new System.Drawing.Size(199, 232);
            this.lstComponentsAvailable.TabIndex = 0;
            // 
            // cmdAddComponents
            // 
            this.cmdAddComponents.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.cmdAddComponents.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAddComponents.FlatAppearance.BorderSize = 0;
            this.cmdAddComponents.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAddComponents.Font = new System.Drawing.Font("Bahnschrift", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdAddComponents.ForeColor = System.Drawing.Color.White;
            this.cmdAddComponents.Location = new System.Drawing.Point(223, 109);
            this.cmdAddComponents.Name = "cmdAddComponents";
            this.cmdAddComponents.Size = new System.Drawing.Size(128, 55);
            this.cmdAddComponents.TabIndex = 10;
            this.cmdAddComponents.Text = "<< ajouter";
            this.cmdAddComponents.UseVisualStyleBackColor = false;
            this.cmdAddComponents.Click += new System.EventHandler(this.cmdAddComponents_Click);
            // 
            // cmdRemoveComponents
            // 
            this.cmdRemoveComponents.BackColor = System.Drawing.Color.Red;
            this.cmdRemoveComponents.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdRemoveComponents.FlatAppearance.BorderSize = 0;
            this.cmdRemoveComponents.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRemoveComponents.Font = new System.Drawing.Font("Bahnschrift", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdRemoveComponents.ForeColor = System.Drawing.Color.White;
            this.cmdRemoveComponents.Location = new System.Drawing.Point(223, 170);
            this.cmdRemoveComponents.Name = "cmdRemoveComponents";
            this.cmdRemoveComponents.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cmdRemoveComponents.Size = new System.Drawing.Size(128, 55);
            this.cmdRemoveComponents.TabIndex = 10;
            this.cmdRemoveComponents.Text = "retirer >>";
            this.cmdRemoveComponents.UseVisualStyleBackColor = false;
            this.cmdRemoveComponents.Click += new System.EventHandler(this.cmdRemoveComponents_Click);
            // 
            // FrmOrder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.ClientSize = new System.Drawing.Size(598, 556);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.grpPasta);
            this.Controls.Add(this.grpInfo);
            this.Controls.Add(this.cmdCancelOrder);
            this.Controls.Add(this.cmdValidateOrder);
            this.Font = new System.Drawing.Font("Bahnschrift", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmOrder";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Commande";
            this.grpInfo.ResumeLayout(false);
            this.grpInfo.PerformLayout();
            this.grpPasta.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.grpComponentsChosen.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button cmdValidateOrder;
        private System.Windows.Forms.Button cmdCancelOrder;
        private System.Windows.Forms.GroupBox grpInfo;
        private System.Windows.Forms.Label lblNbOrder;
        private System.Windows.Forms.Label lblLabelNbOrder;
        private System.Windows.Forms.Label lblPrice;
        private System.Windows.Forms.Label lblLabelPrice;
        private System.Windows.Forms.GroupBox grpPasta;
        private System.Windows.Forms.ComboBox cmbPastaType;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListBox lstComponentsChosen;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ListBox lstComponentsAvailable;
        private System.Windows.Forms.GroupBox grpComponentsChosen;
        private System.Windows.Forms.Button cmdRemoveComponents;
        private System.Windows.Forms.Button cmdAddComponents;
    }
}
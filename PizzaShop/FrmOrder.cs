﻿using System.Collections.Generic;
using System.Windows.Forms;
using ModelPizzaShop;

namespace PizzaShop
{
    public partial class FrmOrder : Form
    {


        public FrmOrder()
        {
            InitializeComponent();
            OrderManagement orders = new OrderManagement();
            List<Components> listComponents = null;
            List<string> pastaTypeName = null;
            listComponents = orders.GetComponentsName() ;
            pastaTypeName = orders.GetPastaTypeName();
            foreach (var item in listComponents)
            {
                Components component = new Components(item.Id, item.Name, item.Price);
                listComponents.Add(component);
                lstComponentsAvailable.Items.Add(component.Name);
            }
            foreach (var item in pastaTypeName)
            {
                cmbPastaType.Items.Add(item.ToString());
            }
            cmbPastaType.SelectedIndex = 0;
        }

        private void cmdRemoveComponents_Click(object sender, System.EventArgs e)
        {

        }

        private void cmdAddComponents_Click(object sender, System.EventArgs e)
        {

        }

        private void cmbPastaType_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            OrderManagement orders = new OrderManagement();
            List<string> pastaTypePrice = null;
            pastaTypePrice = orders.GetPastaTypePrice(cmbPastaType.SelectedIndex);
            lblPrice.Text = pastaTypePrice[0] + " CHF";
        }
    }
}

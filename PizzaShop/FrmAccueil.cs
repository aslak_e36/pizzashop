﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using ModelPizzaShop;

namespace PizzaShop
{
    public partial class FrmAccueil : Form
    {
        public FrmAccueil()
        {
            InitializeComponent();
            OrderManagement orders = new OrderManagement();
            List<string> data = null;
            data = orders.GetOrders();
            foreach (var item in data)
            {
                lstOrders.Items.Add("Commande n° " + item);
            }
        }

        private void cmdCloseApp_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void cmdNewOrder_Click(object sender, EventArgs e)
        {
            FrmOrder frmOrder = new FrmOrder();
            DialogResult dialogResult = frmOrder.ShowDialog();
        }
    }
}

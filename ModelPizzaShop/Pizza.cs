﻿using System;
using System.Collections.Generic;

namespace ModelPizzaShop
{
    public class Pizza
    {
        private int num;
        private int pastaType;
        private float price;
        private List<Components> listOfComponents;

        public Pizza(int num, int pastaType, List<Components> listOfComponents)
        {

        }

        public int Num
        {
            get { return this.num; }
        }

        public int PastaType
        {
            get { return this.pastaType; }
            set { this.pastaType = value; }
        }

        public float Price
        {
            get { return this.price; }
        }

        public List<Components> ListOfComponents
        {
            set { this.listOfComponents = value; }
        }
    }
}

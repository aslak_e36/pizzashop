﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ModelPizzaShop
{
    public class Components
    {
        private int id;
        private string name;
        private float price;

        public Components(int id, string name, float price)
        {
            this.id = id;
            this.name = name;
            this.price = price;
        }

        public int Id
        {
            get { return this.id; }
            set { this.id = value; }
        }

        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }

        public float Price
        {
            get { return this.price; }
            set { this.price = value; }
        }

        public override string ToString()
        {
            return this.name;
        }
    }
}

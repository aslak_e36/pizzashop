﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModelPizzaShop
{
    public class OrderManagement
    {
        public List<string> GetOrders()
        {
            List<string> data = null;
            string q = "SELECT id FROM Pizzas;";
            DbConnector db = new DbConnector();
            data = db.Select(q);
            return data;
        }

        public List<Components> GetComponentsName()
        {
            DbConnector db = new DbConnector();
            Components component = null;
            string q = "SELECT * FROM Components;";
            List<string> data = null;
            List<Components> listComponents = new List<Components>();
            data = db.Select(q);
            
            foreach (var item in data)
            {
                //component = new Components((2, "la", item));
                listComponents.Add(component);
            }
            return listComponents;
        }

        public List<string> GetPastaTypeName()
        {
            DbConnector db = new DbConnector();
            string q = "SELECT name FROM Pasta;";
            List<string> data = null;
            data = db.Select(q);
            return data;
        }

        public List<string> GetPastaTypePrice(int id = -1)
        {
            DbConnector db = new DbConnector();
            string q = "SELECT price FROM Pasta;";
            if (id != -1)
            {
                q = "SELECT price FROM pasta where id = '" + id + "';";
            }
            List<string> data = null;
            data = db.Select(q);
            return data;
        }
    }
}

﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace ModelPizzaShop
{
    public class DbConnector
    {
        private MySqlConnection connection;
        private string server;
        private int port;
        private string database;
        private string uid;
        private string password;

        public DbConnector()
        {
            Initialize();
        }

        private void Initialize()
        {
            server = "localhost";
            port = 3306;
            database = "pizzashop";
            uid = "root";
            password = "toor";
            string connectionString;
            connectionString = "Server="+server+";Port="+port+";Database="+database+";Uid="+uid+";Pwd="+password+";";
            connection = new MySqlConnection(connectionString);
        }

        private bool OpenConnection()
        {
            try
            {
                connection.Open();
                return true;
            }
            catch (MySqlException ex)
            {
                switch (ex.Number)
                {
                    case 0:
                        MessageBox.Show("Cannot connect to server. Contact admin");
                        break;
                    case 1045:
                        MessageBox.Show("Wrong user or password");
                        break;
                }
                return false;
            }
        }

        private bool CloseConnection()
        {
            try
            {
                connection.Close();
                return true;
            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        public List<string> Select(string q)
        {
            List<string> data = null;

            if (OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(q, connection);
                MySqlDataReader dataReader = cmd.ExecuteReader();

                data = new List<string>();

                while (dataReader.Read())
                {
                    data.AddRange(new List<string> { 
                        dataReader["id"].ToString(), 
                        dataReader["name"].ToString(), 
                        dataReader["price"].ToString() 
                    });    
                }
                dataReader.Close();
                CloseConnection();
                return data;
            }
            else
            {
                return data;
            }
        }

        public void Insert(string q)
        {
            if (OpenConnection() == true )
            {
                MySqlCommand cmd = new MySqlCommand(q, connection);
                cmd.ExecuteNonQuery();
                CloseConnection();
            }
        }

        public void Update(string q)
        {
            if (OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(q, connection);
                cmd.ExecuteNonQuery();
                CloseConnection();
            }
        }

        public void Delete(string q)
        {
            if (OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(q, connection);
                cmd.ExecuteNonQuery();
                CloseConnection();
            }
        }
    }
}

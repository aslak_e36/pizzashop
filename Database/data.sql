-- --------------------------------------------------------
-- Hôte :                        127.0.0.1
-- Version du serveur:           8.0.17 - MySQL Community Server - GPL
-- SE du serveur:                Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Listage des données de la table pizzashop.Components : ~0 rows (environ)
DELETE FROM `Components`;
/*!40000 ALTER TABLE `Components` DISABLE KEYS */;
INSERT INTO `Components` (`id`, `name`, `price`) VALUES
	(0, 'Ananas', 2),
	(1, 'Basilic', 0.5),
	(2, 'Champignons', 0.5),
	(3, 'Crème fraîche', 0.5),
	(4, 'Epinards', 0.5),
	(5, 'Poivrons', 1),
	(6, 'Fruits de mer', 3),
	(7, 'Merguezes', 1),
	(8, 'Saumon', 3),
	(9, 'Tomates fraîches', 1),
	(10, 'Tofu', 2),
	(11, 'Poulet', 2),
	(12, 'Supplément gruyère', 0.5);
/*!40000 ALTER TABLE `Components` ENABLE KEYS */;

-- Listage des données de la table pizzashop.pasta : ~0 rows (environ)
DELETE FROM `pasta`;
/*!40000 ALTER TABLE `pasta` DISABLE KEYS */;
INSERT INTO `pasta` (`id`, `name`, `price`) VALUES
	(0, 'Classique', 10),
	(1, 'Sans gluten', 12),
	(2, 'Au mais', 13),
	(3, 'Du chef', 15);
/*!40000 ALTER TABLE `pasta` ENABLE KEYS */;

-- --------------------------------------------------------
-- Hôte :                        127.0.0.1
-- Version du serveur:           8.0.18 - MySQL Community Server - GPL
-- SE du serveur:                Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Listage de la structure de la base pour pizzashop
DROP DATABASE IF EXISTS `pizzashop`;
CREATE DATABASE IF NOT EXISTS `pizzashop` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `pizzashop`;

-- Listage de la structure de la table pizzashop. components
DROP TABLE IF EXISTS `components`;
CREATE TABLE IF NOT EXISTS `components` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `price` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Les données exportées n'étaient pas sélectionnées.

-- Listage de la structure de la table pizzashop. pasta
DROP TABLE IF EXISTS `pasta`;
CREATE TABLE IF NOT EXISTS `pasta` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `price` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Les données exportées n'étaient pas sélectionnées.

-- Listage de la structure de la table pizzashop. pizzas
DROP TABLE IF EXISTS `pizzas`;
CREATE TABLE IF NOT EXISTS `pizzas` (
  `id` int(11) NOT NULL,
  `Pasta_id` int(11) NOT NULL,
  PRIMARY KEY (`id`,`Pasta_id`),
  KEY `fk_Pizzas_Pasta_idx` (`Pasta_id`),
  CONSTRAINT `fk_Pizzas_Pasta` FOREIGN KEY (`Pasta_id`) REFERENCES `pasta` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Les données exportées n'étaient pas sélectionnées.

-- Listage de la structure de la table pizzashop. pizzas_has_components
DROP TABLE IF EXISTS `pizzas_has_components`;
CREATE TABLE IF NOT EXISTS `pizzas_has_components` (
  `Pizzas_id` int(11) NOT NULL,
  `Components_id` int(11) NOT NULL,
  PRIMARY KEY (`Pizzas_id`,`Components_id`),
  KEY `fk_Pizzas_has_Components_Components1_idx` (`Components_id`),
  KEY `fk_Pizzas_has_Components_Pizzas1_idx` (`Pizzas_id`),
  CONSTRAINT `fk_Pizzas_has_Components_Components1` FOREIGN KEY (`Components_id`) REFERENCES `components` (`id`),
  CONSTRAINT `fk_Pizzas_has_Components_Pizzas1` FOREIGN KEY (`Pizzas_id`) REFERENCES `pizzas` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Les données exportées n'étaient pas sélectionnées.

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
